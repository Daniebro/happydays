//
//  MemoryCell.swift
//  HappyDays
//
//  Created by Danni Brito on 5/26/20.
//  Copyright © 2020 Danni Brito. All rights reserved.
//

import UIKit

class MemoryCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    
}
